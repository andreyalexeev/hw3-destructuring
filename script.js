'use strict';

// tasks 1

const uniqueClients = [...new Set([...clients1, ...clients2])];

console.log(uniqueClients);

// task 2

const charactersShortInfo = characters.map(({ name, lastName, age }) => ({ name, lastName, age }));

console.log(charactersShortInfo);

// task 3

const { name: ім_я, years: вік, isAdmin = false } = user1;

console.log(`Ім'я: ${ім_я}`);
console.log(`Вік: ${вік}`);
console.log(`isAdmin: ${isAdmin}`);

// task 4

const fullProfile = {
    ...satoshi2018,
    ...satoshi2019,
    ...satoshi2020
};

console.log(fullProfile);

// task 5

const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
};

const updatedBooks = [...books, bookToAdd];

console.log(updatedBooks);

// task 6

const fullEmployee = {
    ...employee,
    age: 30,
    salary: 50000
};

console.log(fullEmployee);

// task 7

const array = ['value', () => 'showValue'];

const [value, showValue] = array;

alert(value); 
alert(showValue());  
